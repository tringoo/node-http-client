function HttpClient(){
	var self = this, http = require('http'),https = require('https'),fs = require('fs'),ctype={'audio/mpeg':'.mp3','image/jpeg':'.jpg'};
	this._cookie={};
	this._isjson=function(obj) {
		var r = typeof(obj) == "object" && Object.prototype.toString.call(obj).toLowerCase() == "[object object]" && !obj.length;    
		return r;
	}
	this.request = function(_url,_method,_data,_sucfun,_dir,_header){
		var _lastName = self.__lastName(_url);
		var _protocol = self.__protocol(_url);
		var _options = self._parseOption(_url,_method,_header);

		var poststr = null;
		if(_method='POST' &&  _data!=null){
			poststr = _data;
			if(self._isjson(_data)){
				poststr = JSON.stringify(_data);
				_options.headers["Content-Type"]='application/json;charset=UTF-8';
			}else if(_data.indexOf('=')!=-1){
				_options.headers["Content-Type"]='application/x-www-form-urlencoded;charset=UTF-8';
			}else if(_data.indexOf('{')!=-1 && _data.indexOf(':')!=-1){
				_options.headers["Content-Type"]='application/json;charset=UTF-8';
			}
			_options.headers["Content-Length"]=poststr.length;
		}
		var client = self.__doRequest(_lastName,_protocol,_options,_sucfun,_dir);
		client.on('error',function(e) {
			if(_sucfun) _sucfun('exception:'+e.message,500);
			else console.log('problem with request: ' + e.message);
		});
		client.on('need_redirect',function(url) {
			console.log('redirect to：'+url);
			self.get(url,_sucfun,_dir);
		});
		if(poststr!=null)
			client.write(poststr);
		client.end();
	};
	this.__doRequest = function(_lastName,_protocol,_options,_sucfun,_dir) {
		var _client = _protocol.request(_options,function(response){
			  var set_cookie = response.headers['set-cookie'];
			  if(set_cookie)
			  	self._cookie[_options.headers['Host']] += set_cookie;
			  console.log('['+response.statusCode+'] < -- '+_options.path);
			  var istxt=true,result ='',isRedirect = response.statusCode >= 300 && response.statusCode < 400;
			  if (!isRedirect){
				  var contentType = response.headers['content-type'];
				  contentType = contentType?contentType:'';
				  if(contentType.indexOf('image/')==0 || contentType.indexOf('audio/')==0 
				  || contentType.indexOf('video/')==0 || contentType.indexOf('application/octet-stream')==0){
					  _lastName = _lastName.indexOf('.')==-1? _lastName+ctype[contentType]:_lastName;
					  console.log('start download：'+_lastName+' to：'+_dir);
					  istxt = false;
					  response.setEncoding('binary');
				  }else{
					  response.setEncoding('utf8');
				  }
			  }
			  response.on('data', function (chunk) {
					result += chunk;
			  });
			  response.on('end',function(){
				  if(isRedirect){
					  var reurl = response.headers['location'];
					  if(reurl && reurl.indexOf('/')==0){
						 reurl = _options.headers['Referer']+reurl;
					  	 reurl = reurl.replace(/\/\//g,'\/').replace(':/','://');
					  }
					  if(!reurl)
					  	reurl = result.substring(18+result.indexOf('redirected you to '));
					  _client.emit("need_redirect",reurl);
					  return;
				  }
				 if(istxt==false){
					if(!_dir){
						 console.log('not find download directory!');
						 return;
					}
					var _stream = new Buffer(result,'binary');
					fs.writeFile(_dir+'/'+_lastName, _stream, function(e) {
						if(e) console.log(e);
						else console.log('end download');
					});
					return;
				 }
			    if(response.statusCode==200)
			    	if(_sucfun)_sucfun(null,result,response);
			    else if(_sucfun)
			    	_sucfun('error:'+response.statusCode,result,response);
			  });
		});
		return _client;
	};
	this.get = function(_url,_sucfun,_dir,_header){
		self.request(_url,'GET',null,_sucfun,_dir,_header);
	};
	this.post = function(_url,_data,_sucfun,_dir,_header){
		self.request(_url,'POST',_data,_sucfun,_dir,_header);
	};
	this._parseOption = function(_url,_method,_header){
		var _host = self.__host_port(_url);
		if(!self._cookie[_host])
			self._cookie[_host]='';
		var _options = {
			method:_method,
			host:self.__host(_url),
			port:self.__port(_url),
			path:self.__path(_url),
			headers:{
			'Accept':'*/*',
			'Accept-Language':'en-US,zh-cn;q=0.5',
			'Host':_host,
			'Referer':self.__pro_host_port(_url),
			'Connection':'Keep-Alive',
			'Cookie':self._cookie[_host] || '',
			//'X-Requested-With':'XMLHttpRequest',
			'Accept-Charset':'GBK,utf-8;q=0.7,*;q=0.3',
			'User-Agent':'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.802.30 Safari/535.1 SE 2.X MetaSr 1.0'}
		};
		if(_header){
			for(var hnm in _header){
				if(_header[hnm]) _options.headers[hnm]=_header[hnm];
			}
		}
		return _options;
	};
	this.__lastName = function(__url){
		var m = __url.match(/.*[^\/]\/([^\.^\?^\/]+\.[^\.^\?^\/]+)\??/i);
		m = m?m:__url.match(/.*\/([^\?^\/]+)/i);
		return m?m[1]:null;
	}
	this.__protocol = function(__url){
		return __url.substring(0,__url.indexOf(":")) === 'https'? https:http;
	};
	this.__host_port = function(__url){
		var hostPattern = /\w+:\/\/([^\/]+)(\/)?/i;
		var domain = __url.match(hostPattern);
		return domain[1];
	};
	this.__pro_host_port = function(__url){
		var hostPattern = /\w+:\/\/([^\/]+)(\/)?/i;
		var domain = __url.match(hostPattern);
		return domain[0];
	};
	this.__host = function(__url){
		var hostPattern = /\w+:\/\/([^\/]+)(\/)?/i;
		var domain = __url.match(hostPattern);
		var pos = domain[1].indexOf(":");
		if(pos !== -1)
			domain[1] = domain[1].substring(0, pos);
		return domain[1];
	};
	this.__port = function(__url){
		var portPattern = /\w+:\/\/([^\/]+)(\/)?/i;
		var domain = __url.match(portPattern);
		var pos = domain[1].indexOf(":");
		if(pos !== -1) {
			domain[1] = domain[1].substr(pos + 1);
			return parseInt(domain[1]);
		} else if(__url.toLowerCase().substr(0, 5) === "https")
			return 443;
		else return 80;
	};
	this.__path = function(__url){
		var pathPattern = /\w+:\/\/([^\/]+)(\/.+)(\/$)?/i;
		var fullPath = __url.match(pathPattern);
		return fullPath?fullPath[2]:'/';
	};
}
module.exports=HttpClient;